import { BehaviorSubject, Observable } from 'rxjs';
import { distinctUntilChanged, pluck } from 'rxjs/operators/';
import { Category, Label, Note } from './core';
import { Injectable } from "@angular/core";

export interface State {
  user: any;
  categories: Category[];
  notes: Note[];
  filteredNotesBy: number;
  editNote: number;
  labels: Label[];
  [key: string]: any;
}

// default state
const initState: State = {
  user: JSON.parse(localStorage.getItem('user')),
  notes: [],
  filteredNotesBy: 0,
  editNote: undefined,
  categories: [],
  labels: []
};

// create list of sections store which we can select
export enum Section {
  user = 'user',
  filteredNotesBy = 'filteredNotesBy',
  editNote = 'editNote',
  notes = 'notes',
  categories = 'categories',
  labels = 'labels'
}

@Injectable()
export class Store {
  private subject = new BehaviorSubject<any>(initState);
  private store = this.subject.asObservable().pipe(distinctUntilChanged());

  get value() {
    return this.subject.value;
  }

  select<T>(name: string): Observable<T> {
    return this.store.pipe(pluck(name));
  }

  set(name: string, newState: any) {
    this.subject.next({ ...this.value, [name]: newState });
    // console.log('NEW UPDATE::::', this.value);
  }
}
