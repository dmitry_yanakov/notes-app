import { Component, OnInit } from '@angular/core';
import { Store, Section } from 'src/app/store';
import { ApiService, Note, Label } from 'src/app/core';
import { switchMap } from 'rxjs/operators';
import { notEqual } from 'assert';

@Component({
  selector: 'app-notes-container',
  templateUrl: './notes-container.component.html',
  styleUrls: ['./notes-container.component.scss']
})
export class NotesContainerComponent implements OnInit {

  notes: Note[] = [];
  notesFiltered: Note[] = [];
  filter = 0;
  labels: Label[] = [];
  labelsFiltered: Label[] = [];

  constructor(private store: Store, private apiService: ApiService) { }

  ngOnInit() {
    this.apiService.getNotes().subscribe();
    this.store.select<number>(Section.filteredNotesBy).subscribe(filter => {
      this.filter = filter;
    });
    this.store.select<Note[]>(Section.notes).subscribe(notes => {
      this.notes = [...notes];
      this.notesFiltered = +this.filter ? this.notes.filter(note => note.categoryId.includes(this.filter)) : this.notes;
    });
    this.store.select<Label[]>(Section.labels).subscribe(labels => {
      this.labels = [...labels];
    });
  }

  delete(id) {
    this.apiService.deleteNote(id).subscribe();
  }

  edit(id) {
    this.store.set(Section.editNote, id);
  }

  getLabels(labelsIds: number[]) {
    return this.labels.filter(label => labelsIds.includes(label.id));
  }
}
