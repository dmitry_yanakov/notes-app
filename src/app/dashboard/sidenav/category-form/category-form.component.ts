import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/core';

@Component({
  selector: 'app-category-form',
  templateUrl: './category-form.component.html',
  styleUrls: ['./category-form.component.scss']
})
export class CategoryFormComponent implements OnInit {

  constructor( private apiService: ApiService ) { }

  ngOnInit() {
  }

  addCategory(name) {
    this.apiService.createCategory(name).subscribe();
  }
}
