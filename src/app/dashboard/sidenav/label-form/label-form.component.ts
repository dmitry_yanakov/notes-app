import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Colors } from '../../colors';
import { ApiService } from 'src/app/core';

@Component({
  selector: 'app-label-form',
  templateUrl: './label-form.component.html',
  styleUrls: ['./label-form.component.scss']
})
export class LabelFormComponent implements OnInit {

  constructor( private apiService: ApiService ) { }

  colors = [...Colors];

  labelForm = new FormGroup({
    name: new FormControl('', [Validators.required]),
    color: new FormControl(this.colors[0], [Validators.required])
  });

  ngOnInit() {
  }

  addLabel() {
    this.apiService.createLabels(this.labelForm.value).subscribe();
  }
}
