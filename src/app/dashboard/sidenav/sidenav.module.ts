import { ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import { AppMaterialModule } from '../../app-material/app-material.module';
import { SidenavComponent } from './sidenav.component';
import { CategoryFormComponent } from './category-form/category-form.component';
import { CategoryListComponent } from './category-list/category-list.component';
import { LabelFormComponent } from './label-form/label-form.component';
import { LabelListComponent } from './label-list/label-list.component';
import { NotesContainerComponent } from '../notes-container/notes-container.component';
import { NoteFormComponent } from '../note-form/note-form.component';

@NgModule({
  imports: [
    ReactiveFormsModule,
    AppMaterialModule
  ],
  declarations: [
    SidenavComponent,
    CategoryFormComponent,
    CategoryListComponent,
    LabelFormComponent,
    LabelListComponent,
    CategoryFormComponent,
    CategoryListComponent,
    NotesContainerComponent,
    NoteFormComponent
  ],
  providers: [],
  exports: [SidenavComponent]
})
export class SidenavModule { }
