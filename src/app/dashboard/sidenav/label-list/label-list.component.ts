import { Component, OnInit } from '@angular/core';
import { Store, Section } from 'src/app/store';
import { ApiService, Label } from 'src/app/core';

@Component({
  selector: 'app-label-list',
  templateUrl: './label-list.component.html',
  styleUrls: ['./label-list.component.scss']
})
export class LabelListComponent implements OnInit {

  labels: Label[] = [];

  constructor(private store: Store, private apiService: ApiService) { }

  ngOnInit() {
    this.apiService.getLabels().subscribe();
    this.store.select<Label[]>(Section.labels).subscribe(labels => this.labels = labels);
  }

  delete(id) {
    this.apiService.deleteLabel(id).subscribe();
  }
}
