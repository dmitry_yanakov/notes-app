import { Component, OnInit } from '@angular/core';
import { Store, Section } from 'src/app/store';
import { ApiService, Category } from 'src/app/core';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.scss']
})
export class CategoryListComponent implements OnInit {

  categories: Category[] = [];
  selected = '0';

  constructor(private store: Store, private apiService: ApiService) { }

  ngOnInit() {
    this.apiService.getCategories().subscribe();
    this.store.select<Category[]>(Section.categories).subscribe(categories => this.categories = categories);
  }

  filterNotes(id) {
    this.store.set(Section.filteredNotesBy, id);
  }

  delete(id) {
    this.apiService.deleteCategory(id).subscribe();
    this.selected = '0';
  }
}
