import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { AppMaterialModule } from '../app-material/app-material.module';
import { SidenavModule } from './sidenav/sidenav.module';


@NgModule({
  imports: [
    CommonModule,
    DashboardRoutingModule,
    AppMaterialModule,
    SidenavModule
  ],
  declarations: [DashboardComponent]
})
export class DashboardModule { }
