import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Store, Section } from "src/app/store";
import { ApiService, Category, Label, Note } from "src/app/core";
import { Colors } from "../colors";

@Component({
  selector: 'app-note-form',
  templateUrl: './note-form.component.html',
  styleUrls: ['./note-form.component.scss']
})
export class NoteFormComponent implements OnInit {
  constructor(private store: Store, private apiService: ApiService) { }

  isOpen = false;
  panelOpenState = false;
  categories: Category[] = [];
  labels: Label[] = [];
  colors = [...Colors];
  filter = 0;
  notes: Note[] = [];
  editNote: Note[] = [];
  selected: any;

  noteForm = new FormGroup({
    title: new FormControl("", [Validators.required]),
    body: new FormControl("", [Validators.required]),
    color: new FormControl(this.colors[0], [Validators.required]),
    categoryId: new FormControl(""),
    labels: new FormControl("")
  });

  ngOnInit() {
    this.store
      .select<Category[]>(Section.categories)
      .subscribe(categories => (this.categories = categories));
    this.store
      .select<Label[]>(Section.labels)
      .subscribe(labels => (this.labels = labels));
    this.store.select<number>(Section.editNote).subscribe(filter => {
      this.filter = filter;
      if (+this.filter) {
        this.store.select<Note[]>(Section.notes).subscribe(notes => {
          this.notes = [...notes];
          this.editNote = +this.filter
            ? this.notes.filter(note => note.id === this.filter)
            : this.notes;
        });
        this.loadNoteForm();
      }
    });
  }

  get getForm(): FormGroup {
    return this.noteForm;
  }

  addNote() {
    this.apiService.createNote(this.noteForm.value).subscribe();
    this.closedForm();
  }

  closedForm() {
    this.store.set(Section.editNote, undefined);
    this.noteForm.reset();
    this.isOpen = false;
  }

  loadNoteForm() {
    this.noteForm.setValue({
      title: this.editNote[0].title,
      body: this.editNote[0].body,
      color: this.editNote[0].color,
      categoryId: this.editNote[0].categoryId,
      labels: this.editNote[0].labels
    });
    this.isOpen = true;
  }

  changeNote() {
    this.noteForm.value.id = this.editNote[0].id;
    this.noteForm.value.userId = this.editNote[0].userId;
    this.apiService.editNote(this.noteForm.value).subscribe();
  }
}
