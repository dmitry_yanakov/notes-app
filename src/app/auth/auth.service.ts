import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { User } from './user';
import { environment } from '../../environments/environment';
import { Store, Section } from '../store';
import { first, tap } from 'rxjs/operators';
import { ApiService } from '../core';

const apiUrl = environment.apiUrl;

@Injectable()
export class AuthService {

  userData;

  constructor(private router: Router, private http: HttpClient,  private store: Store, private apiService: ApiService) {

  }

  registration(user: User) {
    return this.http.post(`${apiUrl}/users`, {email: user.email, password: user.password});
  }


  login(user: User) {
    return this.http.get(`${apiUrl}/users?email=${user.email}`).pipe(first(), tap( userData => {
      if (userData[0] && userData[0].password === user.password) {
        localStorage.setItem('user', JSON.stringify(userData[0]));
        this.store.set(Section.user, userData[0]);
        this.userData = userData[0];
        this.router.navigate(['/dashboard']);
      } else {
        this.apiService.openSnackBar('Email or Password is incorrect', 'Error');
      }
    }
    ));
  }

  logout() {
    localStorage.removeItem('user');
    this.store.set(Section.user, null);
    this.router.navigate(['/login']);
  }
}
