import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { map, first, tap } from 'rxjs/operators';

import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Category, Label, Note } from '../models/note.model';
import { Store, Section } from 'src/app/store';
import { MatSnackBar } from '@angular/material/snack-bar';

const apiUrl = environment.apiUrl;

@Injectable()
export class ApiService {

  constructor(private http: HttpClient, private store: Store, private snackBar: MatSnackBar) {}

  private get id() { return this.store.value.user.id; }

  openSnackBar(message: string, action = 'Success') {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  getUserId() {
    return this.store.value.user.id;
  }
  // CATEGORIES
  getCategories() {
    return this.http.get<Category[]>(`${apiUrl}/categories?userId=${this.id}`).pipe(first(), tap(categories => {
      this.store.set(Section.categories, categories);
    }));
  }

  createCategory(name) {
    const userId = this.id;
    return this.http.post<Category>(`${apiUrl}/categories`, { name, userId }).pipe(first(), tap(category => {
      const categories = [...this.store.value.categories];
      categories.push(category);
      this.store.set(Section.categories, categories);
      this.openSnackBar('Category was successfully created');
    }));
  }

  deleteCategory(id) {
    return this.http.delete<Category>(`${apiUrl}/categories/${id}`).pipe(first(), tap(() => {
      const categories = this.store.value.categories.filter(c => c.id !== id );
      this.store.set(Section.categories, categories);
      this.openSnackBar('Category was successfully deleted');
    }));
  }
  // END CATEGORIES

  // LABELS
  getLabels() {
    return this.http.get<Label[]>(`${apiUrl}/labels?userId=${this.id}`).pipe(first(), tap(labels => {
      this.store.set(Section.labels, labels);
    }));
  }

  createLabels(payLoad: Label) {
    payLoad.userId = this.id;
    return this.http.post<Label>(`${apiUrl}/labels`, payLoad).pipe(first(), tap(label => {
      const labels = [...this.store.value.labels];
      labels.push(label);
      this.store.set(Section.labels, labels);
      this.openSnackBar('Label was successfully created');
    }));
  }

  deleteLabel(id) {
    return this.http.delete<Label>(`${apiUrl}/labels/${id}`).pipe(first(), tap(() => {
      const labels = this.store.value.labels.filter(l => l.id !== id );
      this.store.set(Section.labels, labels);
      this.openSnackBar('Label was successfully deleted');
    }));
  }
  // END LABELS

  // get NOTES
  getNotes() {
    return this.http.get<Note[]>(`${apiUrl}/notes?userId=${this.id}`).pipe(first(), tap(notes => {
      this.store.set(Section.notes, notes);
    }));
  }

  createNote(payLoad: Note) {
    payLoad.userId = this.id;
    return this.http.post<Note>(`${apiUrl}/notes`, payLoad).pipe(first(), tap(note => {
      const notes = [...this.store.value.notes];
      notes.push(note);
      this.store.set(Section.notes, notes);
      this.openSnackBar('Note was successfully created');
    }));
  }

  editNote(payLoad: Note) {
    return this.http.put<Note>(`${apiUrl}/notes/${payLoad.id}`, payLoad).pipe(first(), tap(note => {
      const notes = this.store.value.notes.filter(n => n.id !== payLoad.id );
      notes.push(note);
      this.store.set(Section.notes, notes);
      this.openSnackBar('Note was successfully edited');
    }));
  }

  deleteNote(id) {
    return this.http.delete<Note>(`${apiUrl}/notes/${id}`).pipe(first(), tap(() => {
      const notes = this.store.value.notes.filter(n => n.id !== id );
      this.store.set(Section.notes, notes);
      this.openSnackBar('Note was successfully deleted');
    }));
  }
}
