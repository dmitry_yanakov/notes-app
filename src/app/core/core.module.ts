import { NgModule } from '@angular/core';
import { ApiService } from './services';

@NgModule({
  imports: [
  ],
  providers: [
    ApiService
  ],
  declarations: []
})
export class CoreModule { }
